import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { User } from '../interfaces/user';

@Injectable({
  providedIn: 'root'
})

export class UsersService {
  public user:User = {
    id: -1,
    name: "",
    username: "",
    email: "",
    address: {
        street: "",
        suite: "",
        city: "",
        zipcode: "",
        geo: {
        lat: "",
        lng: ""
        }
    },
    phone: "",
    website: "",
    company: {
        name: "",
        catchPhrase: "",
        bs: ""
    }
  };
  public UrlAPI:string = "https://jsonplaceholder.typicode.com/users/";

  constructor(private http:HttpClient, public storage:Storage) {
    this.getUser().then((user:User)=>{
      this.user = user;
    })
  }
  public setUser(user){
    this.storage.set("user",user).then(()=>{
      this.user = user
    })
  }
  public getUser(){
    return new Promise((response)=>{
      this.storage.get("user").then((user)=>{
        if(user){
          response(user)
        }else{
          this.getUserById(1).then((user)=>{
            this.setUser(user)
            response(user)
          })
        }
      })
    })
  }
  public getAllUsers():any{
    return new Promise((response)=>{
      this.http.get(this.UrlAPI).subscribe((users:any)=>{
        response(users)
      })
    })
  }
  public getUserById(id:number):any{
    return new Promise((response)=>{
      this.http.get(this.UrlAPI + id).subscribe((user:User)=>{
        response(user)
      })
    })
  }
  public updateUser(user,id){
    return new Promise((response)=>{
      this.http.put(this.UrlAPI + id, JSON.stringify(user)).subscribe((res)=>{
        console.log(res)
        this.setUser(user)
        response(true)
      })
    }) 
      
  }
}
