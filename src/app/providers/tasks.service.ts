import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TasksService {
  public UrlAPI:string = "https://jsonplaceholder.typicode.com/users/1/todos";
  constructor(private http:HttpClient) { }

  public getAllTasks():any{
    return new Promise((response)=>{
      this.http.get(this.UrlAPI).subscribe((tasks:any)=>{
        response(tasks)
      })
    })
  }
}
