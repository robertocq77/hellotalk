import { Component, OnInit } from '@angular/core';
import { PostsService } from '../../providers/posts.service';
import { Post } from '../../interfaces/post';
import { UsersService } from '../../providers/users.service';
import { User } from '../../interfaces/user';
import { AlertController, LoadingController, ToastController, ModalController } from '@ionic/angular';
import { PostDetailPage } from '../post-detail/post-detail.page';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  public posts: Array<Post> = [];
  public users: any;
  constructor(public postService:PostsService, 
              public usersService:UsersService,
              public loadingController: LoadingController,
              public toastController: ToastController,
              public modalController: ModalController,
              public alertController: AlertController) { }

  ngOnInit() {
    this.loadPosts(3)
  }
  public loadData($event:any){
    this.loadPosts(2);
    $event.target.complete();
  }
  public openComment(postIndex,commentIndex){
    let comment = this.posts[postIndex].comments[commentIndex];
    this.presentAlert(comment.name,comment.email,comment.body)
  }
  public loadPosts(number){
    for (let i = this.posts.length; i <= this.posts.length + number; i++) {
      this.postService.getPostById(i + 1).then((data:Post)=>{
        let post = data;
        this.postService.getComentaries(post.id).then((comments:any)=>{ 
          post.comments = comments; 
          this.usersService.getUserById(data.userId).then((user:User)=>{
            post.author = {name: user.name, username: user.username} 
            this.posts.push(post)
          })
        })
      })
      
    }
  }
  public createPost(){
    this.presentModal({title:"",body:""},false,true)
  } 
  public deletePost(id,index){
    let buttons = [
      {
        text: "Delete",
        handler: async ()=>{
          const loading = await this.loadingController.create({
            message: 'Deleting post...',
          });
          await loading.present().then(()=>{
            this.postService.deletePost(id).then(()=>{
              loading.dismiss();
              this.posts.splice(index,1)
              this.presenToast("The post has deleted")
            })
          })
        }
      },
      {
        text: "Cancel"
      }
    ]
    this.presentAlert("Delete Post", this.posts[id].title,this.posts[id].body, buttons)
  }
  public editPost(index){
    this.presentModal(this.posts[index],index)
  }
  public async presentModal(post?,index?,create?) {
    const modal = await this.modalController.create({
      component: PostDetailPage,
      cssClass: 'my-custom-class',
      componentProps: {
        post: post,
        create : create || false,
      },
    });
    modal.onDidDismiss().then((res)=>{
      if(res.data){
        if(create){
          this.usersService.getUser().then((user:User)=>{
            let post = res.data
            post["author"] = {name: user.name, username: user.username},
            post["userId"] = user.id
            post["id"] = 1;
            this.posts.unshift(post)
          })
          this.presenToast("The Post has created!")
        }else{
          this.presenToast("The Post has updated!")
        }
      }else{
        if(res.data == false)
        this.posts.splice(index,1)
      }
    })
    return await modal.present();
  }
  public async presenToast(message, duration?){
    let toast = await this.toastController.create({
      message: message,
      duration: duration || 5000,
    })
    return await toast.present()
  }
  public async presentAlert(title,subtitle,body,buttons?) {
    const alert = await this.alertController.create({
      header: title,
      subHeader: subtitle,
      message: body,
      buttons: buttons || ['Close']
    });

    await alert.present();
  }
}
