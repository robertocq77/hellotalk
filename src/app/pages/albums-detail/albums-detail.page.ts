import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PhotosService } from '../../providers/photos.service';
import { Photo } from '../../interfaces/photo';

@Component({
  selector: 'app-albums-detail',
  templateUrl: './albums-detail.page.html',
  styleUrls: ['./albums-detail.page.scss'],
})
export class AlbumsDetailPage implements OnInit {
  public photos:Array<Photo> = [];
  public counts = [1,2,3,4,5,6,7,8];
  constructor(public route: ActivatedRoute,public photosService:PhotosService) { }

  ngOnInit() {

  }
  ionViewWillEnter() {
    let id = this.route.snapshot.paramMap.get('albumId');
    this.photosService.getAllPhotos(id).then((photos:any)=>{
      this.photos = photos;
    })
  }
}
