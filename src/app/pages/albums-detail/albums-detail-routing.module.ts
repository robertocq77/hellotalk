import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AlbumsDetailPage } from './albums-detail.page';

const routes: Routes = [
  {
    path: '',
    component: AlbumsDetailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AlbumsDetailPageRoutingModule {}
