import { Component, OnInit } from '@angular/core';
import { AlbumsService } from '../../providers/albums.service';
import { Album } from '../../interfaces/album';
import { PhotosService } from '../../providers/photos.service';
import { Photo } from '../../interfaces/photo';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.page.html',
  styleUrls: ['./albums.page.scss'],
})
export class AlbumsPage implements OnInit {
  public albums:Array<Album> = [];
  public counts = [1,2,3,4,5,6,7,8];
  constructor(public albumsService:AlbumsService, public photosService:PhotosService) { }

  ngOnInit() {
    setTimeout(()=>{
      this.albumsService.getAllAlbums().then((albums:any)=>{
        for (let i = 0; i < albums.length; i++) {
          let album:Album = albums[i];
          this.photosService.getPhotoById(i).then((photo:Photo)=>{
            album.photo = photo.thumbnailUrl;
            this.albums.push(album)
          })
        }
      })
    },0)
  }

}
