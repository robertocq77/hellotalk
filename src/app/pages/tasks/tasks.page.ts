import { Component, OnInit } from '@angular/core';
import { TasksService } from '../../providers/tasks.service';
import { Task } from '../../interfaces/task';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.page.html',
  styleUrls: ['./tasks.page.scss'],
})
export class TasksPage implements OnInit {
  public tasks:Array<Task> = [];
  public counts = [1,2,3,4,5,6,7];
  constructor(public tasksService:TasksService) { }

  ngOnInit() {
    this.tasksService.getAllTasks().then((tasks)=>{
      this.tasks = tasks;
    })
  }
}
